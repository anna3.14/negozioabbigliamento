import java.util.HashMap;
import java.util.Map;

public class Ordine {
    private Cliente cliente;
    Map<Prodotto,Integer> elencoOrdine;
    private int numeroOrdine;


    public Cliente getCliente() {
        return cliente;
    }

    public Map<Prodotto,Integer> getElencoOrdine() {
        return elencoOrdine;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    //Costruttore
    public Ordine(Cliente cliente, int  numeroOrdine){
        this.cliente=cliente;
        elencoOrdine=new HashMap<>();
        this.elencoOrdine=elencoOrdine;
        this.numeroOrdine=numeroOrdine;
    }

    public int getNumeroOrdine() {
        return numeroOrdine;
    }

    public void setNumeroOrdine(int numeroOrdine) {
        this.numeroOrdine = numeroOrdine;
    }
}

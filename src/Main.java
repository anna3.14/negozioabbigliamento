//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {
        Negozio negozio=new Negozio();
        Maglia maglia=new Maglia("a3d4","maglia","bella e azzurra",10,100);
        Pantaloni pantaloni=new Pantaloni("bb34","pantaloni","corti e leggeri",25,150);
        Scarpe scarpe=new Scarpe("vg12","scarpe","estive",43,50);

        //Aggiungo Prodotto a stock
        negozio.aggiungiProdotto(maglia);
        negozio.aggiungiProdotto(pantaloni);
        negozio.aggiungiProdotto(scarpe);

        //Visualizza prodotto in stock
        negozio.visualizzaProdottiDisponibili();

        System.out.println("---------------------------------------------------");
        //Rimozione prodotto
        negozio.rimuoviProdotto("vg12");
        System.out.println("Post rimozione: ");
        negozio.visualizzaProdottiDisponibili();


        System.out.println("--------------------------------------------------");

        Cliente anna= new Cliente("3456","anna",25);
        Cliente giacomo=new Cliente("4533","Giacomo",33);
        Ordine ordine1=new Ordine(anna,345279);

        //Aggiungo cliente in storico clienti
        negozio.aggiungiCliente(anna);
        negozio.aggiungiCliente(giacomo);
        negozio.visualizzaClienti();

        System.out.println("-------------------------------------------------------");
        //Rimuovo clienti
        negozio.rimuoviCliente("3456");
        System.out.println("Post rimozione cliente: ");
        negozio.visualizzaClienti();


        System.out.println("--------------------------------------------------");

        //Aggiungo ordine in storico
        negozio.aggiungiOrdine(ordine1,anna);
        negozio.visualizzaOrdini();

    }
}
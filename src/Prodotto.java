public abstract class Prodotto {
    private String codiceProdotto;
    private String tipo;
    private String descrizione;
    private float prezzo;
    private int articoliDisponibili;

    public String getCodiceProdotto() {
        return codiceProdotto;
    }

    public void setCodiceProdotto(String codiceProdotto) {
        this.codiceProdotto = codiceProdotto;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescrizione() {
        return descrizione;
    }

    public void setDescrizione(String descrizione) {
        this.descrizione = descrizione;
    }

    public float getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(float prezzo) {
        this.prezzo = prezzo;
    }

    public int getArticoliDisponibili() {
        return articoliDisponibili;
    }

    public void setArticoliDisponibili(int articoliDisponibili) {
        this.articoliDisponibili = articoliDisponibili;
    }

    //Costruttore
    public Prodotto(String codiceProdotto, String tipo, String descrizione, float prezzo,int articoliDisponibili){
        this.codiceProdotto=codiceProdotto;
        this.tipo=tipo;
        this.descrizione=descrizione;
        this.prezzo=prezzo;
        this.articoliDisponibili=articoliDisponibili;
    }
}

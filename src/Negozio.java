import java.util.HashMap;
import java.util.Map;

public class Negozio {
    Map<String, Integer> stock; //Codice prodotto chiave/quantità valore
    Map<String,Cliente> storicoClienti; //key Codice cliente / value: cliente --> tengo traccia

    Map<Integer,Cliente> storicoOrdini;//key Codice ordine/value cliente;

    public Map<String, Integer> getStock() {
        return stock;
    }

    public Map<String,Cliente> getStoricoClienti() {
        return storicoClienti;
    }

    //Costruttore
    public Negozio(){
        Map<String, Integer> stock=new HashMap<>();
        Map<String,Cliente> storicoClienti= new HashMap<>();
        Map<Integer,Cliente> storicoOrdini= new HashMap<>();

        this.stock=stock;
        this.storicoClienti=storicoClienti;
        this.storicoOrdini=storicoOrdini;

    }

    //Metodo per aggiungere un nuovo prodotto
    public void aggiungiProdotto(Prodotto prodotto){
        stock.put(prodotto.getCodiceProdotto(), prodotto.getArticoliDisponibili());
    }

    //Rimuovi prodotto
    public void rimuoviProdotto(String codiceProdotto){
        String stringa=null;
        for(Map.Entry<String, Integer> entry:stock.entrySet()){
            if(entry.getKey()==codiceProdotto){
                stringa=codiceProdotto;
            }
        }
        stock.remove(stringa);
    }
    //Aggiungi cliente
    public void aggiungiCliente(Cliente cliente){
        storicoClienti.put(cliente.getCodice(), cliente);
    }

    //Aggiungere Ordine
    public void aggiungiOrdine(Ordine ordine,Cliente cliente){
        storicoOrdini.put(ordine.getNumeroOrdine(), cliente);

    }

    //Rimuovi cliente

    public void rimuoviCliente(String codiceCliente){
        String stringa=null;
        for(Map.Entry<String,Cliente> entry:storicoClienti.entrySet()){
            if(entry.getKey()==codiceCliente){
              stringa=codiceCliente;
            }
        }
        storicoClienti.remove(stringa);
    }

    //Visualizza prodotti disponibili
    public void visualizzaProdottiDisponibili(){
        for(Map.Entry<String,Integer> entry:stock.entrySet()){
            System.out.println("Codice prodotto: "+entry.getKey());
            if(entry.getValue()>0){
            System.out.println("Quantità: "+entry.getValue());}

        }
    }

    //Visualizza clienti registrati
    public void visualizzaClienti(){
        for(Map.Entry<String,Cliente> entry:storicoClienti.entrySet()){
            System.out.println("Codice cliente: "+entry.getKey());
            System.out.println("Nome cliente: "+entry.getValue().getNome()+"\n"+"Età cliente: "+entry.getValue().getEta());

        }
    }

    //Visualizza ordini
    public void visualizzaOrdini(){
        for(Map.Entry<Integer,Cliente> entry: storicoOrdini.entrySet()){
            System.out.println("Codice ordine: "+entry.getKey());
            System.out.println("Nome cliente: "+entry.getValue().getNome()+"\n"+"Età cliente: "+entry.getValue().getEta());
        }
    }

}



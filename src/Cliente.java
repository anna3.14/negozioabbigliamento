public class Cliente {
    private String codice;//alfanumerico
    private String nome;
    private int eta;

    public String getCodice() {
        return codice;
    }

    public void setCodice(String codice) {
        this.codice = codice;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getEta() {
        return eta;
    }

    public void setEta(int eta) {
        this.eta = eta;
    }

    //Costruttore
    public Cliente(String codice, String nome, int eta){
        this.codice=codice;
        this.nome=nome;
        this.eta=eta;
    }
}
